/****************************************************************************************************************************************
*-Autor: Gregoire BARRET																												
*-Date: Jan 14, 2014																														
*-Project: Smarto																												
*-File: DrawerListAdaptater.java, DrawerListAdaptater																										
*-Content: 																															
***************************************************************************************************************************************/
package com.smartsoftasia.smarto.adaptater;

import java.util.List;

import com.smartsoftasia.smarto.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author slooblack
 * @file DrawerListAdaptater.java
 * @date Jan 14, 2014
 * @todo TODO
 */
public class DrawerListAdaptater extends BaseAdapter {
	
	private LayoutInflater inflater;
	private List<String> items;
	private Context context;
	
	
	public class ViewHolder {
		TextView textview_title;
		ImageView imageview_icone;

	}
	
    /*******************************************************************/
    /***********				Constructor					************/
    /*******************************************************************/

	public DrawerListAdaptater(List<String> items, Context context) {
		this.items = items;
		this.inflater = LayoutInflater.from(context);
		this.context = context;
	}
	
    /*******************************************************************/
    /***********			Get Position items				************/
    /*******************************************************************/

	@Override
	public int getCount() {
		return this.items.size();
	}

	@Override
	public Object getItem(int position) {
		return this.items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

    /*******************************************************************/
    /***********			Get Position views				************/
    /*******************************************************************/

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.drawer_list_item , parent, false);
			holder.textview_title = (TextView)convertView.findViewById(R.id.textview_drawer);
			holder.imageview_icone = (ImageView)convertView.findViewById(R.id.imageView_drawer);
            
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}


		holder.textview_title.setText(items.get(position));
		holder.imageview_icone.setImageResource(R.drawable.ic_geofencing);


		return convertView;

	}

}
