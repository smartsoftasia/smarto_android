package com.smartsoftasia.smarto;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.smartsoftasia.smarto.adaptater.DrawerListAdaptater;
import com.smartsoftasia.smarto.fragment.ClaimsSurveyorFragment;
import com.smartsoftasia.smarto.fragment.ECallFragment;
import com.smartsoftasia.smarto.fragment.GeoFencingFragment;
import com.smartsoftasia.smarto.fragment.MoveAlertFragment;
import com.smartsoftasia.smarto.fragment.PanicAlertFragment;
import com.smartsoftasia.smarto.fragment.RoadsideAssistanceFragment;
import com.smartsoftasia.smarto.fragment.ServiceAlertFragment;
import com.smartsoftasia.smarto.fragment.SpeedCheckFragment;
import com.smartsoftasia.smarto.fragment.ThreftRecoveryFragment;
import com.smartsoftasia.smarto.fragment.TrackFragment;
import com.smartsoftasia.smarto.fragment.WhereIsMyCarFragment;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.SearchManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {

	    private DrawerLayout mDrawerLayout;
	    private ListView mDrawerList;
	    private ActionBarDrawerToggle mDrawerToggle;
	    private DrawerListAdaptater mDrawerListAdaptater;

	    private CharSequence mDrawerTitle;
	    private CharSequence mTitle;
	    private List<String> mViewTitles;

	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.main_activity);

	        mTitle = mDrawerTitle = getTitle();
	        mViewTitles = Arrays.asList(getResources().getStringArray(R.array.view_array));
	        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_main_activity);
	        mDrawerList = (ListView) findViewById(R.id.left_drawer_main_activity);

	        // set a custom shadow that overlays the main content when the drawer opens
	        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
	        // set up the drawer's list view with items and click listener
	        mDrawerListAdaptater = new DrawerListAdaptater(mViewTitles,this);
	        mDrawerList.setAdapter(mDrawerListAdaptater);
	        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

	        // enable ActionBar app icon to behave as action to toggle nav drawer
	        getActionBar().setDisplayHomeAsUpEnabled(true);
	        getActionBar().setHomeButtonEnabled(true);

	        // ActionBarDrawerToggle ties together the the proper interactions
	        // between the sliding drawer and the action bar app icon
	        mDrawerToggle = new ActionBarDrawerToggle(
	                this,                  /* host Activity */
	                mDrawerLayout,         /* DrawerLayout object */
	                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
	                R.string.drawer_open,  /* "open drawer" description for accessibility */
	                R.string.drawer_close  /* "close drawer" description for accessibility */
	                ) {
	            public void onDrawerClosed(View view) {
	                getActionBar().setTitle(mTitle);
	                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
	            }

	            public void onDrawerOpened(View drawerView) {
	                getActionBar().setTitle(mDrawerTitle);
	                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
	            }
	        };
	        mDrawerLayout.setDrawerListener(mDrawerToggle);

	        if (savedInstanceState == null) {
	            selectItem(0);
	        }
	    }

	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.main, menu);
	        return super.onCreateOptionsMenu(menu);
	    }

	    /* Called whenever we call invalidateOptionsMenu() */
	    @Override
	    public boolean onPrepareOptionsMenu(Menu menu) {
	        // If the nav drawer is open, hide action items related to the content view
	        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
	        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
	        menu.findItem(R.id.action_share).setVisible(!drawerOpen);
	        menu.findItem(R.id.action_cars).setVisible(!drawerOpen);
	        return super.onPrepareOptionsMenu(menu);
	    }

	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	         // The action bar home/up action should open or close the drawer.
	         // ActionBarDrawerToggle will take care of this.
	        if (mDrawerToggle.onOptionsItemSelected(item)) {
	            return true;
	        }
	        // Handle action buttons
	        switch(item.getItemId()) {
	        case R.id.action_settings:
	              Toast.makeText(this, "action", Toast.LENGTH_LONG).show();
	            return true;
	        case R.id.action_share:
	              Toast.makeText(this, "share", Toast.LENGTH_LONG).show();
	            return true;
	        case R.id.action_cars:
	              Toast.makeText(this, "cars", Toast.LENGTH_LONG).show();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	        }
	    }

	    /* The click listner for ListView in the navigation drawer */
	    private class DrawerItemClickListener implements ListView.OnItemClickListener {
	        @Override
	        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	            selectItem(position);
	        }
	    }

	    private void selectItem(int position) {
	        // update the main content by replacing fragments
	        Fragment fragment;
	        Bundle args = new Bundle();
	        
	        switch (position) {
			case 0:
				fragment = new ClaimsSurveyorFragment();
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM1, String.valueOf(position));
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM2, String.valueOf(position));
		        fragment.setArguments(args);		
				break;
			case 1:
				fragment = new RoadsideAssistanceFragment();
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM1, String.valueOf(position));
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM2, String.valueOf(position));
		        fragment.setArguments(args);		
				break;
			case 2:
				fragment = new PanicAlertFragment();
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM1, String.valueOf(position));
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM2, String.valueOf(position));
		        fragment.setArguments(args);		
				break;
			case 3:
				fragment = new MoveAlertFragment();
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM1, String.valueOf(position));
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM2, String.valueOf(position));
		        fragment.setArguments(args);		
				break;
			case 4:
				fragment = new SpeedCheckFragment();
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM1, String.valueOf(position));
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM2, String.valueOf(position));
		        fragment.setArguments(args);		
				break;
			case 5:
				fragment = new ECallFragment();
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM1, String.valueOf(position));
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM2, String.valueOf(position));
		        fragment.setArguments(args);		
				break;
			case 6:
				fragment = new ThreftRecoveryFragment();
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM1, String.valueOf(position));
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM2, String.valueOf(position));
		        fragment.setArguments(args);		
				break;
			case 7:
				fragment = new ServiceAlertFragment();
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM1, String.valueOf(position));
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM2, String.valueOf(position));
		        fragment.setArguments(args);		
				break;
			case 8:
				fragment = new WhereIsMyCarFragment();
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM1, String.valueOf(position));
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM2, String.valueOf(position));
		        fragment.setArguments(args);		
				break;
			case 9:
				fragment = new TrackFragment();
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM1, String.valueOf(position));
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM2, String.valueOf(position));
		        fragment.setArguments(args);		
				break;
			case 10:
				fragment = new GeoFencingFragment();
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM1, String.valueOf(position));
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM2, String.valueOf(position));
		        fragment.setArguments(args);		
				break;

			default:
				fragment = new ClaimsSurveyorFragment();
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM1, String.valueOf(position));
		        args.putString(ClaimsSurveyorFragment.ARG_PARAM2, String.valueOf(position));
		        fragment.setArguments(args);
				break;
			}
	        


	        FragmentManager fragmentManager = getFragmentManager();
	        fragmentManager.beginTransaction().replace(R.id.content_frame_main_activity, fragment).commit();

	        // update selected item and title, then close the drawer
	        mDrawerList.setItemChecked(position, true);
	        setTitle(mViewTitles.get(position));
	        mDrawerLayout.closeDrawer(mDrawerList);
	    }

	    @Override
	    public void setTitle(CharSequence title) {
	        mTitle = title;
	        getActionBar().setTitle(mTitle);
	    }

	    /**
	     * When using the ActionBarDrawerToggle, you must call it during
	     * onPostCreate() and onConfigurationChanged()...
	     */

	    @Override
	    protected void onPostCreate(Bundle savedInstanceState) {
	        super.onPostCreate(savedInstanceState);
	        // Sync the toggle state after onRestoreInstanceState has occurred.
	        mDrawerToggle.syncState();
	    }

	    @Override
	    public void onConfigurationChanged(Configuration newConfig) {
	        super.onConfigurationChanged(newConfig);
	        // Pass any configuration change to the drawer toggls
	        mDrawerToggle.onConfigurationChanged(newConfig);
	    }
    

}
